﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReadIT.Models
{
    public class Poll
    {
        [Key]
        public int PollId { get; set; }

        [Required(ErrorMessage = "The field 'Title' is required.")]
        public string Title { get; set; }

        [Required(ErrorMessage = "The field 'Description' is required.")]
        public string Description { get; set; }

        public PollType PollType { get; set; }

        public int Votes { get; set; }

        public string Author { get; set; }

        public virtual List<Comment> Comments { get; set; }

        [Required(ErrorMessage = "The field 'Content' is required.")]
        public string Content { get; set; }

        public DateTime PublicationDate { get; set; }
    }

    public enum PollType
    {
        Link,
        Text,
        Image,
        Video
    }
}