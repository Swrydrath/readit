﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReadIT.Models
{
    public class PollVote
    {
        [Key]
        public int PollVoteId { get; set; }

        public int PollId { get; set; }

        public string UserName { get; set; }

        public bool Vote { get; set; }
    }
}