﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReadIT.Models
{
    public class CommentVote
    {
        [Key]
        public int CommentVoteId { get; set; }

        public int CommentId { get; set; }

        public string UserName { get; set; }

        public bool Vote { get; set; }
    }
}