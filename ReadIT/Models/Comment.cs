﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReadIT.Models
{
    public class Comment
    {
        [Key]
        public int CommentId { get; set; }

        public int PollId { get; set; }

        [Required(ErrorMessage = "The field 'Comments' cannot be empty.")]
        public string Content { get; set; }

        public string Author { get; set; }

        public DateTime PublicationDate { get; set; }

        public int Votes { get; set; }
    }
}