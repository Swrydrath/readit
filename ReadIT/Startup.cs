﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ReadIT.Startup))]
namespace ReadIT
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
