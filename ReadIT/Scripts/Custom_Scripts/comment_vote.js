﻿$(document).ready(function () {

    var vote_counter = 0; 

    var user_name = $('#userName').data('username');

    // read comment votes
    $.ajax({
        //url: "/ControllerName/ActionName",
        url: '/Polls/GetComentatorCommentIds/gvvfu/' + user_name,
        success: function (data) {

            var keysValues = data.split(":", 2);

            var keys = keysValues[0].split(";"); 
            var values = keysValues[1].split(";"); 
            var length = keys.length;

            for (var i = 0; i < length; i++) {
                var commentId = keys[i];

                if (values[i] == "True") {
                    $("#upvote\\:" + commentId).addClass("plus_btn_pressed");
                    $("#downvote\\:" + commentId).removeClass("minus_btn_pressed");
                }
                else if (values[i] == "False") {
                    $('#upvote\\:' + commentId).removeClass("plus_btn_pressed");
                    $('#downvote\\:' + commentId).addClass("minus_btn_pressed");
                }
            }
        },
        error: function () {
            alert('error');
        }
    });

    $(".plus_btn").click(function () {

        var button_id = $(this).attr('id');
        var valuesFromId = button_id.split(":", 2);
        var comment_id = valuesFromId[1];      

        var author = $("#upvote\\:" + comment_id).data("author");

        if (author == user_name) {
            alert("You cannot vote for your own comment.")
            return;
        }

        var noVote = !$("#upvote\\:" + comment_id).hasClass("plus_btn_pressed") && !$("#downvote\\:" + comment_id).hasClass("minus_btn_pressed");
        var upvote = $("#upvote\\:" + comment_id).hasClass("plus_btn_pressed");
        var downvote = $("#downvote\\:" + comment_id).hasClass("minus_btn_pressed");

        if (noVote)
            vote_counter = 1;
        else if (downvote)
            vote_counter = 2;
        else if (upvote)
            return;
                
        $.ajax({            
            url: '/Polls/CommentVote/' + comment_id + "/" + vote_counter + "/" + user_name,
            success: function () {
                var id_value = "vote_value_comment" + comment_id;
                var vote_value = $("#" + id_value).text();
                var new_vote_value = parseInt(vote_value) + vote_counter;

                $("#" + id_value).text(new_vote_value);

                vote_counter = 1;

                $("#upvote\\:" + comment_id).addClass("plus_btn_pressed");
                $("#downvote\\:" + comment_id).removeClass("minus_btn_pressed");
            }
        });
    });

    $(".minus_btn").click(function () {

        var button_id = $(this).attr('id');
        var valuesFromId = button_id.split(":", 2);
        var comment_id = valuesFromId[1];

        var author = $("#downvote\\:" + comment_id).data("author");

        if (author == user_name) {
            alert("You cannot vote for your own comment.")
            return;
        }

        var noVote = !$("#upvote\\:" + comment_id).hasClass("plus_btn_pressed") && !$("#downvote\\:" + comment_id).hasClass("minus_btn_pressed");
        var upvote = $("#upvote\\:" + comment_id).hasClass("plus_btn_pressed");
        var downvote = $("#downvote\\:" + comment_id).hasClass("minus_btn_pressed");

        if (noVote)
            vote_counter = -1;
        else if (upvote)
            vote_counter = -2;
        else if (downvote)
            return;

        $.ajax({            
            url: '/Polls/CommentVote/' + comment_id + "/" + vote_counter + "/" + user_name,
            success: function () {
                var id_value = "vote_value_comment" + comment_id;
                var vote_value = $("#" + id_value).text();
                var new_vote_value = parseInt(vote_value) + vote_counter;

                $("#" + id_value).text(new_vote_value);

                vote_counter = -1;

                $("#downvote\\:" + comment_id).addClass("minus_btn_pressed");
                $("#upvote\\:" + comment_id).removeClass("plus_btn_pressed");
            }
        });
    });
});