﻿$(document).ready(function () {

    var vote_counter = 0;

    var user_name = $('#userName').data('username');

    $.ajax({
        url: '/Polls/GetComentatorPollIds/gvvfu/' + user_name,
        success: function (data) {            

            var keysValues = data.split(":",2);

            var keys = keysValues[0].split(";");
            var values = keysValues[1].split(";");
            var length = keys.length;

            for (var i = 0; i < length; i++)
            {
                var pollId = keys[i];

                if (values[i] == "True")
                {
                    $("#PollVoteUp\\:" + pollId).removeClass("vote_up_btn").addClass("vote_up_btn_pressed");
                    $("#PollVoteDown\\:" + pollId).removeClass("vote_down_btn_pressed").addClass("vote_down_btn");
                }
                else if (values[i] == "False")
                {
                    $('#PollVoteUp\\:' + pollId).removeClass("vote_up_btn_pressed").addClass("vote_up_btn");
                    $('#PollVoteDown\\:' + pollId).removeClass("vote_down_btn").addClass("vote_down_btn_pressed");
                }
            }
        },
        error: function () {
            alert('error');
        }
    });

    $(".vote_up_btn").click(function () {               

        var buttonId = $(this).attr('id');
        var valuesFromId = buttonId.split(":", 2);
        var pollId = valuesFromId[1];

        var author = $("#Author\\:" + pollId).data("author");

        if (author == user_name) {
            alert("You cannot vote for your own poll.")
            return;
        }

        var noVote = !$("#PollVoteUp\\:" + pollId).hasClass("vote_up_btn_pressed") && !$("#PollVoteDown\\:" + pollId).hasClass("vote_down_btn_pressed");
        var upvote = $("#PollVoteUp\\:" + pollId).hasClass("vote_up_btn_pressed");
        var downvote = $("#PollVoteDown\\:" + pollId).hasClass("vote_down_btn_pressed");

        if (noVote)
            vote_counter = 1;
        else if (downvote)
            vote_counter = 2;
        else if (upvote)
            return;
        
        $.ajax({
            url: '/Polls/PollVote/' + pollId + "/" + vote_counter + "/" + user_name,
            success: function () {                

                var id_value = "vote_value" + pollId;
                var vote_value = $("#" + id_value).text();
                var new_vote_value = parseInt(vote_value) + vote_counter;

                $("#" + id_value).text(new_vote_value);

                vote_counter = 1;                

                $("#PollVoteUp\\:" + pollId).removeClass("vote_up_btn").addClass("vote_up_btn_pressed");
                $("#PollVoteDown\\:" + pollId).removeClass("vote_down_btn_pressed").addClass("vote_down_btn");
            }
        });
    });

    $(".vote_down_btn").click(function () {

        var buttonId = $(this).attr('id');
        var valuesFromId = buttonId.split(":", 2);
        var pollId = valuesFromId[1];

        var author = $("#Author\\:" + pollId).data("author");

        if (author == user_name) {
            alert("You cannot vote for your own poll.")
            return;
        }

        var noVote = !$("#PollVoteUp\\:" + pollId).hasClass("vote_up_btn_pressed") && !$("#PollVoteDown\\:" + pollId).hasClass("vote_down_btn_pressed");
        var upvote = $("#PollVoteUp\\:" + pollId).hasClass("vote_up_btn_pressed");
        var downvote = $("#PollVoteDown\\:" + pollId).hasClass("vote_down_btn_pressed");

        if (noVote)
            vote_counter = -1;
        else if (upvote)
            vote_counter = -2;
        else if (downvote)
            return;

        $.ajax({
            url: '/Polls/PollVote/' + pollId + "/" + vote_counter + "/" + user_name,
            success: function () {
                
                var id_value = "vote_value" + pollId;
                var vote_value = $("#" + id_value).text();
                var new_vote_value = parseInt(vote_value) + vote_counter;

                $("#" + id_value).text(new_vote_value);

                vote_counter = -1;

                $('#PollVoteUp\\:' + pollId).removeClass("vote_up_btn_pressed").addClass("vote_up_btn");
                $('#PollVoteDown\\:' + pollId).removeClass("vote_down_btn").addClass("vote_down_btn_pressed");
            }
        });
    });
});