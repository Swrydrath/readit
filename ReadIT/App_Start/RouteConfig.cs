﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ReadIT
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Polls", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "Vote",
            url: "{controller}/{action}/{id}/{voteValue}/{userName}"
            );

            routes.MapRoute(
                name: "GetVoteValuesForUser",
                url: "{controller}/{action}/gvvfu/{userName}" //gvvfu - Shorthand of GetVoteValuesForUser
            );
        }
    }
}
