﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReadIT.Models;
using Microsoft.AspNet.Identity;
using System.IO;

namespace ReadIT.Controllers
{
    public class PollsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index(string searchString)
        {
            var polls = from p in db.Polls
                         select p;

            if (!String.IsNullOrEmpty(searchString))
            {
                polls = polls.Where(s => s.Title.Contains(searchString));
                return View(polls.ToList());
            }
            return View(db.Polls.ToList());
        }

        // GET: Polls/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Poll poll = db.Polls.Find(id);
            if (poll == null)
            {
                return HttpNotFound();
            }

            return View(poll);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Details(int? id, FormCollection form)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Poll poll = db.Polls.Find(id);
            if (poll == null)
            {
                return HttpNotFound();
            }

            var content = form["Comment"];

            Comment comm = new Comment();
            comm.Content = content;
            comm.Author = User.Identity.Name;
            comm.PollId = poll.PollId;
            comm.Votes = 0;
            comm.PublicationDate = DateTime.Now;

            if (poll.Comments == null)
                poll.Comments = new List<Comment>();

            poll.Comments.Add(comm);

            db.SaveChanges();

            return View(poll);
        }

        // GET: Polls/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Polls/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "PollId,Title")] Poll poll)
        {
            if (ModelState.IsValid)
            {
                poll.Author = "";
                poll.Votes = 0;

                db.Polls.Add(poll);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(poll);
        }

        // GET: Polls/Create
        [Authorize]
        public ActionResult CreateText()
        {
            return View();
        }

        // POST: Polls/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult CreateText([Bind(Include = "PollId,Title,Description,Content")] Poll poll)
        {
            if (ModelState.IsValid)
            {
                poll.Author = User.Identity.GetUserName();
                poll.Votes = 0;
                poll.PollType = PollType.Text;
                poll.PublicationDate = DateTime.Now;

                db.Polls.Add(poll);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(poll);
        }

        // GET: Polls/Create
        [Authorize]
        public ActionResult CreateLink()
        {
            Poll poll = new Poll();
            poll.Content = "http://";
            return View(poll);
        }

        // POST: Polls/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult CreateLink([Bind(Include = "PollId,Title,Description,Content")] Poll poll)
        {
            if (ModelState.IsValid)
            {
                poll.Author = User.Identity.GetUserName();
                poll.Votes = 0;
                poll.PollType = PollType.Link;
                poll.PublicationDate = DateTime.Now;

                db.Polls.Add(poll);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(poll);
        }

        // GET: Polls/Create
        [Authorize]
        public ActionResult CreateImage()
        {
            return View();
        }

        // POST: Polls/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult CreateImage([Bind(Include = "PollId,Title,Description")] Poll poll, HttpPostedFileBase file)
        {
            // manually check if model is valid
            bool isValid = false;
            if (poll.PollId != null && poll.Title != null && poll.Description != null)
                isValid = true;

            if (isValid)
            {
                poll.Author = User.Identity.GetUserName();
                poll.Votes = 0;
                poll.PollType = PollType.Image;
                poll.PublicationDate = DateTime.Now;

                // Image file
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);

                    var path = Path.Combine(Server.MapPath("~/Content/Images"), fileName);
                    file.SaveAs(path);
                    poll.Content = fileName;
                }
                else
                {
                    return View(poll);
                }

                db.Polls.Add(poll);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(poll);
        }

        // GET: Polls/Create
        [Authorize]
        public ActionResult CreateVideo()
        {
            return View();
        }

        // POST: Polls/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult CreateVideo([Bind(Include = "PollId,Title,Description,Content")] Poll poll)
        {
            if (ModelState.IsValid)
            {
                poll.Author = User.Identity.GetUserName();
                poll.Votes = 0;
                poll.PollType = PollType.Video;
                poll.PublicationDate = DateTime.Now;

                db.Polls.Add(poll);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(poll);
        }

        // GET: Polls/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Poll poll = db.Polls.Find(id);
            if (poll == null)
            {
                return HttpNotFound();
            }
            return View(poll);
        }

        // POST: Polls/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PollId,Title,Votes,Author")] Poll poll)
        {
            if (ModelState.IsValid)
            {
                db.Entry(poll).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(poll);
        }

        // GET: Polls/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Poll poll = db.Polls.Find(id);
            if (poll == null)
            {
                return HttpNotFound();
            }
            return View(poll);
        }

        // POST: Polls/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Poll poll = db.Polls.Find(id);
            db.Polls.Remove(poll);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Polls/PollVote/5
        public void PollVote(int? id, int voteValue, string userName)
        {
            // id - poll Id
            Poll poll = db.Polls.Find(id);

            if (poll != null)
            {
                PollVote pv = db.PollVotes.FirstOrDefault(u => (u.PollId == (int)id) && (u.UserName == userName));

                if (pv == null)
                {
                    pv = new Models.PollVote();
                    db.PollVotes.Add(pv);
                }

                pv.PollId = (int)id;
                pv.UserName = userName;
                if (voteValue > 0)
                    pv.Vote = true;
                else if (voteValue < 0)
                    pv.Vote = false;

                poll.Votes += voteValue;
                db.SaveChanges();
            }
        }

        // GET: Polls/CommentVoteUp/5
        public void CommentVote(int? id, int voteValue, string userName)
        {
            // id - comment Id
            Comment comm = db.Comments.Find(id);
            if (comm != null)
            {
                CommentVote cv = db.CommentVotes.FirstOrDefault(u => (u.CommentId == (int)id) && (u.UserName == userName));

                if (cv == null)
                {
                    cv = new Models.CommentVote();
                    db.CommentVotes.Add(cv);
                }

                cv.CommentId = (int)id;
                cv.UserName = userName;
                if (voteValue > 0)
                    cv.Vote = true;
                else if (voteValue < 0)
                    cv.Vote = false;

                comm.Votes += voteValue;
                db.SaveChanges();
            }
        }

        // GET: Polls/GetComentatorPollIds/test
        public string GetComentatorPollIds(string userName)
        {
            string returnPollId_Vote = "";

            var pvQuery = db.PollVotes.Where(pv => pv.UserName == userName);

            if (!pvQuery.Any())
                return "";

            var pvList = pvQuery.ToList();

            foreach (var pv in pvList)
            {
                returnPollId_Vote += pv.PollId.ToString() + ";";
            }

            returnPollId_Vote += ":";

            foreach (var pv in pvList)
            {
                returnPollId_Vote += pv.Vote.ToString() + ";";
            }

            return returnPollId_Vote;
        }

        // GET: Polls/GetComentatorPollIds/test
        public string GetComentatorCommentIds(string userName)
        {
            string returnCommentId_Vote = "";

            var cvQuery = db.CommentVotes.Where(pv => pv.UserName == userName);

            if (!cvQuery.Any())
                return "";

            var pvList = cvQuery.ToList();

            foreach (var pv in pvList)
            {
                returnCommentId_Vote += pv.CommentId.ToString() + ";";
            }

            returnCommentId_Vote += ":";

            foreach (var pv in pvList)
            {
                returnCommentId_Vote += pv.Vote.ToString() + ";";
            }

            return returnCommentId_Vote;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
